/*
 * Copyright 2018 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.redhat;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.transaction.Transaction;
import javax.transaction.TransactionManager;

import org.jbpm.process.workitem.core.AbstractLogOrThrowWorkItemHandler;
import org.jbpm.process.workitem.core.util.RequiredParameterValidator;
import org.jbpm.process.workitem.core.util.Wid;
import org.jbpm.process.workitem.core.util.WidMavenDepends;
import org.jbpm.process.workitem.core.util.WidParameter;
import org.jbpm.process.workitem.core.util.WidResult;
import org.jbpm.process.workitem.core.util.service.WidAction;
import org.jbpm.process.workitem.core.util.service.WidAuth;
import org.jbpm.process.workitem.core.util.service.WidService;
import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Wid(widfile = "SQLWorkItem.wid", name = "SQLWorkItem", displayName = "SQL WorkItem", 
     defaultHandler = "mvel: new com.redhat.SQLWorkItemWorkItemHandler()", documentation = "wokitem-sql/index.html", category = "wokitem-sql", icon = "SQLWorkItem.png", 
     parameters = {
        @WidParameter(name = "SQLStatement", required = true), 
        @WidParameter(name = "MaxResults"), }, 
        results = {
               @WidResult(name = "Result", runtimeType = "java.lang.Object") 
        }, mavenDepends = {
               @WidMavenDepends(group = "${groupId}", artifact = "${artifactId}", version = "${version}") 
        }, serviceInfo = @WidService(category = "${name}", description = "${description}", keywords = "database,fetch,sql,execute", 
                                    action = @WidAction(title = "Execute SQL statements"), 
                                    authinfo = @WidAuth(required = true, params = { "Data source JNDI" },
                                    paramsdescription = { "Data source JNDI" }
        )))
public class SQLWorkItemWorkItemHandler extends AbstractLogOrThrowWorkItemHandler {
    private static final Logger logger = LoggerFactory.getLogger(SQLWorkItemWorkItemHandler.class);
    private static final String RESULT = "Result";
    private static final int DEFAULT_MAX_RESULTS = 10;
    private DataSource ds;
    private int maxResults;
    private TransactionManager txManager;

    public SQLWorkItemWorkItemHandler(String dataSourceName) {
        try {
            this.ds = InitialContext.doLookup(dataSourceName);
            txManager = InitialContext.doLookup("java:jboss/TransactionManager");
        } catch (NamingException e) {
            throw new RuntimeException("Unable to look up data source: " + dataSourceName + " - " + e.getMessage());
        }
    }

    public SQLWorkItemWorkItemHandler(DataSource ds) {
        this.ds = ds;
    }

    public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            RequiredParameterValidator.validate(this.getClass(), workItem);

            Map<String, Object> results = new HashMap<>();
            String sqlStatement = (String) workItem.getParameter("SQLStatement");
            String maxResultsInput = (String) workItem.getParameter("MaxResults");

            maxResults = maxResultsInput != null && !maxResultsInput.trim().isEmpty()
                    ? Integer.parseInt(maxResultsInput)
                    : DEFAULT_MAX_RESULTS;

            try {
                Transaction suspendedTx = txManager.suspend();
                txManager.begin();
                connection = ds.getConnection();
                statement = connection.prepareStatement(sqlStatement);
                statement.setMaxRows(maxResults);

                boolean containsResultSet = statement.execute();
                if (containsResultSet) {
                    resultSet = statement.getResultSet();
                    results.put(RESULT, processResults(resultSet));
                } else {
                    results.put(RESULT, statement.getUpdateCount());
                }
                txManager.commit();
                txManager.resume(suspendedTx);
                workItemManager.completeWorkItem(workItem.getId(), results);
            } finally {
                try {
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            handleException(e);
        }
    }

    // overwrite to implement custom resultset processing
    protected Object processResults(ResultSet resultSet) throws Exception {
        List<Map<String, Object>> results = new ArrayList<>();

        while (resultSet.next()) {
            int columnCount = resultSet.getMetaData().getColumnCount();
            HashMap<String, Object> row = new HashMap<>();
            for (int i = 0; i < columnCount; i++) {
                row.put(resultSet.getMetaData().getColumnName(i + 1), resultSet.getObject(i + 1));
            }
            results.add(row);
        }

        return results;
    }

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        // stub
    }
}
